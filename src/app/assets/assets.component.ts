import { Component, OnInit } from '@angular/core';

interface Food {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-assets',
  templateUrl: './assets.component.html',
  styleUrls: ['./assets.component.css']
})
export class AssetsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  Procurement: Food[] = [
    { value: 'Buy', viewValue: 'Jerry' },
    { value: 'Lease', viewValue: 'Mark' },
    { value: 'Rent', viewValue: 'New Employee' },
  ];

  AssetState: Food[] = [
    { value: 'On Order', viewValue: 'On Order' },
    { value: 'In Store', viewValue: 'On Order' },
    { value: 'In Use', viewValue: 'On Order' },
    { value: 'In Repair', viewValue: 'On Order' },
    { value: 'Missing', viewValue: 'On Order' },
    { value: 'Expired', viewValue: 'On Order' },
    { value: 'Disposed', viewValue: 'On Order' },
  ];

  impact: Food[] = [
    { value: 'High', viewValue: 'Steak' },
    { value: 'Medium', viewValue: 'Steak' },
    { value: 'Low', viewValue: 'Steak' },
  ];

  type: Food[] = [
    { value: 'Laptop', viewValue: 'Steak' },
    { value: 'Printer', viewValue: 'Steak' },
    { value: 'Router', viewValue: 'Steak' },
    { value: 'Tablet', viewValue: 'Steak' },
    { value: 'Smart Phone', viewValue: 'Steak' },
    { value: 'Software', viewValue: 'Steak' },
    { value: 'and so on...', viewValue: 'Steak' },
  ];

  Depreciation: Food[] = [
    { value: 'Not Applicable', viewValue: 'Steak' },
    { value: 'Declining Balance', viewValue: 'Steak' },
    { value: 'Double Declining Balance', viewValue: 'Steak' },
    { value: 'Straight Line', viewValue: 'Steak' },
    { value: 'Smart Phone', viewValue: 'Steak' },
    { value: 'Sum of Years Digits', viewValue: 'Steak' },
  ];

  department: Food[] = [
    {value: 'Finance', viewValue: 'Steak'},
    {value: 'Marketing', viewValue: 'Steak'},
    {value: 'Human Resources', viewValue: 'Steak'},
    {value: 'IT Services', viewValue: 'Steak'},
    {value: 'Information Systems', viewValue: 'Steak'},
    {value: 'IT Infrastructure', viewValue: 'Steak'},
  ];

  location: Food[] = [
    {value: 'Barrie', viewValue: 'Steak'},
    {value: 'Kitchener', viewValue: 'Steak'},
    {value: 'London', viewValue: 'Steak'},
    {value: 'Newmarket', viewValue: 'Steak'},
    {value: 'Ottawa', viewValue: 'Steak'},
    {value: 'Stratford', viewValue: 'Steak'},
    {value: 'Toronto Finch', viewValue: 'Steak'},
    {value: 'Toronto Kennedy', viewValue: 'Steak'},
    {value: 'Windsor', viewValue: 'Steak'},
  ];


}
