import { Component, OnInit } from '@angular/core';

interface Employee {
  value: string;
  viewValue: string;
}

interface Food {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  
  position: Employee[] = [
    {value: 'President', viewValue: 'Jerry'},
    {value: 'Vice-president', viewValue: 'Mark'},
    {value: 'Manager', viewValue: 'New Employee'},
    {value: 'Supervisor', viewValue: 'New Employee'},
    {value: 'Clerk', viewValue: 'New Employee'},

    {value: 'Administrative Assistant', viewValue: 'New Employee'},

    {value: 'Junior Service Desk Analyst', viewValue: 'New Employee'},
    {value: 'Service Desk Analyst', viewValue: 'New Employee'},
    {value: 'Senior Service Desk Analyst', viewValue: 'New Employee'},
    {value: 'Service Desk Supervisor', viewValue: 'New Employee'},
    {value: 'Network Administrator', viewValue: 'New Employee'},
  ];

  location: Food[] = [
    {value: 'Barrie', viewValue: 'Steak'},
    {value: 'Kitchener', viewValue: 'Steak'},
    {value: 'London', viewValue: 'Steak'},
    {value: 'Newmarket', viewValue: 'Steak'},
    {value: 'Ottawa', viewValue: 'Steak'},
    {value: 'Stratford', viewValue: 'Steak'},
    {value: 'Toronto Finch', viewValue: 'Steak'},
    {value: 'Toronto Kennedy', viewValue: 'Steak'},
    {value: 'Windsor', viewValue: 'Steak'},
  ];

  department: Food[] = [
    {value: 'Finance', viewValue: 'Steak'},
    {value: 'Marketing', viewValue: 'Steak'},
    {value: 'Human Resources', viewValue: 'Steak'},
    {value: 'IT Services', viewValue: 'Steak'},
    {value: 'Information Systems', viewValue: 'Steak'},
    {value: 'IT Infrastructure', viewValue: 'Steak'},
  ];

    employee: Food[] = [
      {value: 'Jerry', viewValue: 'Steak'},
      {value: 'Mark', viewValue: 'Steak'},
      {value: 'Tom', viewValue: 'Steak'},
      {value: 'Add New', viewValue: 'Steak'},
  ];

  impact: Food[] = [
    {value: 'High', viewValue: 'Steak'},
    {value: 'Medium', viewValue: 'Steak'},
    {value: 'Low', viewValue: 'Steak'},
];

Tier: Food[] = [
  {value: 'Tier 1 Support', viewValue: 'Steak'},
  {value: 'Tier 2 Support', viewValue: 'Steak'},
  {value: 'Tier 3 Support', viewValue: 'Steak'},
  {value: 'Tier 4 Support', viewValue: 'Steak'},

];

position1: Food[] = [
  {value: 'Junior Service Desk Agent', viewValue: 'Steak'},
  {value: 'Service Desk Agent', viewValue: 'Steak'},
  {value: 'Senior Service Desk Agent', viewValue: 'Steak'},
  {value: 'Service Desk Supervisor', viewValue: 'Steak'},

];

team: Food[] = [
  {value: 'Incident Management Team', viewValue: 'Steak'},
  {value: 'Capacity Management Team', viewValue: 'Steak'},
  {value: 'Change Management Team', viewValue: 'Steak'},
  {value: 'Database Management Team', viewValue: 'Steak'},
  {value: 'more....', viewValue: 'Steak'},

];


Role: Food[] = [
  {value: 'Leader', viewValue: 'Steak'},
  {value: 'Member', viewValue: 'Steak'},
  {value: 'Adviser', viewValue: 'Steak'},


];



}
